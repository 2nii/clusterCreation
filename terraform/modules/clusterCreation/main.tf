terraform {
  required_providers {
    google = {
      source = "hashicorp/google"
      version = "4.28.0"
    }
  }
}

# Input variables declaration, These variables come from the root module 
variable "envName" {
  description = "The name of the environment the cluster will go to"
  type        = string
  default     = "none"
}
variable "clusterName" {
    description = "Name of the cluster"
    type = string
    default = "clusterWithNoName"
}
variable "diskSize" {
    description = "Size of cluster's disks"
    type = number
    default = 10
}


# resource "random_id" "id" {
# 	  byte_length = 8
# }

# Local variables. The module is meant to create a pair of servers
locals {
#   test = join("-", ["master", resource.random_id.id])  # been trying to concatenate a random id to the server's name to avoid conflicts. but since the id is generate by the provider, the script cannot be validate by a simple 'terraform plan'
  srvNames = ["master", "slave"]
  clusterNumber = 2
}

resource "google_compute_instance" "cluster" {
    count        = length(local.srvNames)
    name         = "${element(local.srvNames, count.index)}"
    machine_type = "n1-standard-1"
    zone         = "europe-west1-b"
    tags         = ["mysql"]

    labels       = {
      "clusterName" = var.clusterName
    }

    boot_disk {
        initialize_params {
            image = "debian-cloud/debian-8"
            size  = var.diskSize
        }
    }

    network_interface {
        subnetwork = var.envName
    }

}

